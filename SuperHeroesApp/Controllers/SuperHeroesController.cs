﻿using Microsoft.AspNetCore.Mvc;
using SuperHeroesApp.Models;

namespace SuperHeroesApp.Controllers
{
    public class SuperHeroesController : Controller
    {
        private ISuperHeroeClient _client;

        public SuperHeroesController(ISuperHeroeClient client)
        {
            _client = client;
        }

        public IActionResult Index()
        {
            return View(new SuperHeroeSearchModel());
        }

        [HttpGet]
        public IActionResult Search(SuperHeroeSearchModel model)
        {
            var resultados = _client.SearchSuperHeroe(model.ResultsFor);

            if (resultados.response.Contains("error"))
                TempData["msg"] = resultados.error;

            return View("Index", resultados);
        }
    }
}
