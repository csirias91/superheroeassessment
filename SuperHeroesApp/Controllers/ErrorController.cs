﻿using Microsoft.AspNetCore.Mvc;

namespace SuperHeroesApp.Controllers
{
    public class ErrorController : Controller
    {

        [Route("Error/{statusCode}")]
        public IActionResult HttpStatusResponse(int statusCode)
        {
            return View("NotFound");
        }
    }
}
