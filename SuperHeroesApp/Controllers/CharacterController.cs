﻿using Microsoft.AspNetCore.Mvc;
using SuperHeroesApp.Models;

namespace SuperHeroesApp.Controllers
{
    public class CharacterController : Controller
    {
        private ISuperHeroeClient _client;

        public CharacterController(ISuperHeroeClient client)
        {
            _client = client;
        }

        public IActionResult Index(string Id)
        {
            var resultados = _client.SuperHeroeDetails(Id);

            if (resultados.response.Contains("error"))
            {
                Response.StatusCode = 404;
                return Redirect("~/Error/404");
            }

            return View(resultados);
        }
    }
}
