﻿using Newtonsoft.Json;
using SuperHeroesApp.Models;

namespace SuperHeroesApp
{
    public class SuperHeroeClient : ISuperHeroeClient
    {

        private readonly IHttpUtility _httpUtility;

        public SuperHeroeClient(IHttpUtility httpUtility)
        {
            _httpUtility = httpUtility;
        }
        public SuperHeroeModel SuperHeroeDetails(string superHeroeId) 
        {
            SuperHeroeModel response = null;

            var result = _httpUtility.HttpGet(superHeroeId);

            if (result != null)
                response = JsonConvert.DeserializeObject<SuperHeroeModel>(result.Result);

            return response;
        }

        public SuperHeroeSearchModel SearchSuperHeroe(string superHereo) 
        {
            SuperHeroeSearchModel response = null;

            var result = _httpUtility.HttpGetMethod("search", superHereo);

            if(result != null)
                response = JsonConvert.DeserializeObject<SuperHeroeSearchModel>(result.Result);

            return response;
        }
    }
}
