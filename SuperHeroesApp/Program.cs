using SuperHeroesApp;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllersWithViews();
var services = builder.Services;
var configuration = builder.Configuration;

services.Configure<SuperHeroeAPI>(configuration.GetSection("SuperHeroeAPI"));
services.AddSingleton<ISuperHeroeClient, SuperHeroeClient>();
services.AddSingleton<IHttpUtility, HttpUtility>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseStatusCodePagesWithRedirects("~/Error/{0}");

//app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

app.UseEndpoints(endpoints =>
{
    endpoints.MapControllerRoute(
        name: "character",
        pattern: "Character/{id}",
        defaults: new { controller = "Character", action = "Index" });

    endpoints.MapControllerRoute(
        name: "default",
        pattern: "{controller=SuperHeroes}/{action=Index}/{id?}");
});



app.Run();
