﻿using Newtonsoft.Json;

namespace SuperHeroesApp.Models
{
    public class Appearance
    {
        public string gender { get; set; }
        public string race { get; set; }
        public List<string> height { get; set; }
        public List<string> weight { get; set; }

        [JsonProperty("eye-color")]
        public string EyeColor { get; set; }

        [JsonProperty("hair-color")]
        public string HairColor { get; set; }
    }
}
