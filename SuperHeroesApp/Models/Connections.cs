﻿using Newtonsoft.Json;

namespace SuperHeroesApp.Models
{
    public class Connections
    {
        [JsonProperty("group-affiliation")]
        public string GroupAffiliation { get; set; }
        public string relatives { get; set; }
    }
}
