﻿using Newtonsoft.Json;

namespace SuperHeroesApp.Models
{
    public class SuperHeroeModel
    {
        public string response { get; set; }
        public string id { get; set; }
        public string name { get; set; }
        public Powerstats powerstats { get; set; }
        public Biography biography { get; set; }
        public Appearance appearance { get; set; }
        public Work work { get; set; }
        public Connections connections { get; set; }
        public Image image { get; set; }
    }

    public class SuperHeroeSearchModel 
    {
        public string response { get; set; }
        public string error { get; set; }

        [JsonProperty("results-for")]
        public string ResultsFor { get; set; }
        public List<Result> results { get; set; }
    }
}
