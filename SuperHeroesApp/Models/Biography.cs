﻿using Newtonsoft.Json;

namespace SuperHeroesApp.Models
{
    public class Biography
    {
        [JsonProperty("full-name")]
        public string FullName { get; set; }

        [JsonProperty("alter-egos")]
        public string AlterEgos { get; set; }
        public List<string> aliases { get; set; }

        [JsonProperty("place-of-birth")]
        public string PlaceOfBirth { get; set; }

        [JsonProperty("first-appearance")]
        public string FirstAppearance { get; set; }
        public string publisher { get; set; }
        public string alignment { get; set; }
    }
}
