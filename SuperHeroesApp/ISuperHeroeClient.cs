﻿using SuperHeroesApp.Models;

namespace SuperHeroesApp
{
    public interface ISuperHeroeClient
    {
        SuperHeroeModel SuperHeroeDetails(string superHeroeId);
        SuperHeroeSearchModel SearchSuperHeroe(string superHereo);
    }
}
