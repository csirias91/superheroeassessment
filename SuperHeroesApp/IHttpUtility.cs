﻿namespace SuperHeroesApp
{
    public interface IHttpUtility
    {
        Task<string> HttpGet(string parameter);
        Task<string> HttpGetMethod(string method, string parameter);
    }
}
