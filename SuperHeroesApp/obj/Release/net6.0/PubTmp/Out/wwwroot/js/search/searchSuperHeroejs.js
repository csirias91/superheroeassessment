﻿var superHeroe = superHeroe || {};
superHeroe.Search = function () {

    $('#searchResults').DataTable({
        searching: false
    });

    var ShowValidationSummary = function (errorMessage) {
        if (errorMessage != "") {
            toastr.error(errorMessage, 'Error');
        }
    };

    return {
        ShowValidationSummary: ShowValidationSummary
    }
}();