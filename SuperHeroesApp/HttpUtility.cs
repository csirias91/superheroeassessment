﻿using Microsoft.Extensions.Options;

namespace SuperHeroesApp
{
    public class HttpUtility : IHttpUtility
    {
        private readonly SuperHeroeAPI _apiSetings;
        public HttpUtility(IOptions<SuperHeroeAPI> apiSettings)
        {
            _apiSetings = apiSettings.Value;
        }

        public async Task<string> HttpGet(string parameter)
        {
            HttpResponseMessage response = null;
            using (HttpClient client = new HttpClient())
            {
                string url = $"{_apiSetings.BaseUrl}/{_apiSetings.Token}/{parameter}";
                response = await client.GetAsync(url);
                return await response.Content.ReadAsStringAsync();
            }

        }

        public async Task<string> HttpGetMethod(string method, string parameter)
        {
            HttpResponseMessage response = null;
            using (HttpClient client = new HttpClient())
            {
                string url = $"{_apiSetings.BaseUrl}/{_apiSetings.Token}/{method}/{parameter}";
                response = await client.GetAsync(url);
                return await response.Content.ReadAsStringAsync();
            }
        }
    }
}
