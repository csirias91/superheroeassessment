# Overview
The [SuperHeroeApp](
https://colver-superheroes-assessment.azurewebsites.net/) use [SuperHeroeAPI](https://superheroapi.com/) to make it easy to look for superheroes and their details like:
- Powerstats
- Biography
- Appearance

This assessment was built using ASP .NET Core 6 with Bootstrap and Inspinia theme.

# ASP.NET Core MVC
ASP.NET Core MVC is a rich framework for building web apps and APIs using the Model-View-Controller design pattern.

# Inspinia

It is fully responsive admin dashboard template built with latest Bootstrap v3.3.5 Framework, HTML5, CSS3 and Javascript.

## Installation

Create an App Service using Windows or Linux OS and make sure set [.NET 6](https://dotnet.microsoft.com/en-us/download/dotnet/6.0) runtime.

Now it's hosted on Microsoft Azure using App Services, [SuperHeroeApp](
https://colver-superheroes-assessment.azurewebsites.net/)

## Usage

As per requirements, this web app includes:
1. Initial Search Superheroes Page
2. Superheroes detailed information.